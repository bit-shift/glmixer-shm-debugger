#include <cstdlib>
#include <cstddef>
#include <cassert>
#include <utility>
#include <iostream>
#include <chrono>
#include <thread>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/interprocess/managed_shared_memory.hpp>

int main(int argc, char *argv[])
{
    using namespace boost::interprocess;
    using namespace std::chrono_literals;

    try {
        //Open managed shared memory
        managed_shared_memory segment(open_only, "glmtexture");

        std::pair<uchar*, managed_shared_memory::size_type> shm_matrix;
        std::pair<size_t*, managed_shared_memory::size_type> shm_index;

        while(true)
        {
            // Find the texture and show it
            shm_matrix = segment.find<uchar> ("tex");
            shm_index = segment.find<size_t> ("index");

            auto matrix = shm_matrix.first;
            auto index = shm_index.first;

            if (matrix != nullptr && index != nullptr)
            {
                std::cout << "matrix: [size: " << shm_matrix.second
                          << " value: " << matrix << "]" << std::endl;
                std::cout << "index: [size: " << shm_index.second
                          << " value: " << *index << "]" << std::endl;

                std::this_thread::sleep_for(40ms);
            }

        }
    } catch (boost::interprocess::interprocess_exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
